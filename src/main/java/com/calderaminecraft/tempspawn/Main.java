package com.calderaminecraft.tempspawn;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class Main extends JavaPlugin {
    static Location temporarySpawn;
    static Location delayLoc;
    static int delay;
    static ArrayList players = new ArrayList();
    static Map<UUID, Long> delayedPlayers = new HashMap<>();
    static Main plugin;

    @Override
    public void onEnable() {
        plugin = this;

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        getServer().getPluginManager().registerEvents(new EventListeners(), this);
        this.getCommand("tempspawn").setExecutor(new CommandTempSpawn());

        BukkitTask task = new BukkitRunnable() {
            @Override
            public void run() {
                List<UUID> removeList = new ArrayList<>();
                for (Map.Entry<UUID, Long> entry : delayedPlayers.entrySet()) {
                    long timeEnd = entry.getValue();
                    UUID playerUUID = entry.getKey();
                    if (System.currentTimeMillis() < timeEnd) continue;
                    removeList.add(playerUUID);
                }

                for (UUID uuid : removeList) {
                    Player player = Bukkit.getPlayer(uuid);
                    delayedPlayers.remove(uuid);
                    if (player != null && temporarySpawn != null) player.teleport(temporarySpawn);
                }
            }
        }.runTaskTimer(this, 0, 1 * 20);

        getLogger().info("TempSpawn loaded.");
    }
}
