package com.calderaminecraft.tempspawn;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

public class CommandTempSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("caldera.tempspawn.manage")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.GOLD + "/tempspawn commands:");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn set - Sets a temporary spawn.");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn setdelay <seconds> - Sets a respawn delay, and sets the respawn delay location to your current location.");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn removedelay - Removes the respawn delay.");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn add <player> - Adds the temporary spawn to a player.");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn remove <player> - Removes the temporary spawn from a player.");
            sender.sendMessage(ChatColor.GOLD + "/tempspawn reset - Removes the temporary spawn.");
            return true;
        }

        String targetCmd = args[0];
        if (targetCmd.equals("set")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            Main.temporarySpawn = ((Player) sender).getLocation();
            sender.sendMessage(ChatColor.GOLD + "The temporary spawn has been set to your current location.");
            return true;
        }

        if (targetCmd.equals("setdelay")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /tempspawn setdelay <delay (in seconds)>");
                return true;
            }

            int delay;
            try {
                delay = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid delay.");
                return true;
            }

            Player player = (Player) sender;

            Main.delayLoc = player.getLocation();
            Main.delay = delay;

            sender.sendMessage(ChatColor.GOLD + "The respawn delay has been set to " + delay + (delay == 1 ? " second " : " seconds ") + "and the delay location has been set to your current location.");
            return true;
        }

        if (targetCmd.equals("removedelay")) {
            if (Main.delayLoc == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no respawn delay set.");
                return true;
            }

            Main.delayLoc = null;
            Main.delay = 0;
            Main.delayedPlayers.clear();

            sender.sendMessage(ChatColor.GOLD + "The respawn delay has been removed.");
            return true;
        }

        if (targetCmd.equals("reset")) {
            if (Main.temporarySpawn == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no temporary spawn set.");
                return true;
            }
            for (Map.Entry<UUID, Long> entry : Main.delayedPlayers.entrySet()) {
                long timeEnd = entry.getValue();
                UUID playerUUID = entry.getKey();
                Player curPlayer = Bukkit.getPlayer(playerUUID);
                if (curPlayer != null) curPlayer.teleport(Main.temporarySpawn);
            }
            Main.temporarySpawn = null;
            Main.players.clear();
            Main.delayedPlayers.clear();
            Main.delay = 0;
            Main.delayLoc = null;
            sender.sendMessage(ChatColor.GOLD + "The temporary spawn has been reset.");
            return true;
        }

        if (targetCmd.equals("add")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /tempspawn add <player>");
                return true;
            }

            if (Main.temporarySpawn == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no temporary spawn set.");
                return true;
            }

            Player targetPlayer = Bukkit.getPlayer(args[1]);
            if (targetPlayer == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                return true;
            }

            if (Main.players.contains(targetPlayer.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player has already been added to the temporary spawn.");
                return true;
            }

            Main.players.add(targetPlayer.getUniqueId());
            sender.sendMessage(ChatColor.GOLD + "That player has been added to the temporary spawn.");
            return true;
        }

        if (targetCmd.equals("remove")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /tempspawn remove <player>");
                return true;
            }

            if (Main.temporarySpawn == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no temporary spawn set.");
                return true;
            }

            Player targetPlayer = Bukkit.getPlayer(args[1]);
            if (targetPlayer == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                return true;
            }

            if (!Main.players.contains(targetPlayer.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player has not been added to the temporary spawn.");
                return true;
            }

            Main.players.remove(targetPlayer.getUniqueId());
            sender.sendMessage(ChatColor.GOLD + "That player has been removed from the temporary spawn.");
            return true;
        }

        return false;
    }
}
