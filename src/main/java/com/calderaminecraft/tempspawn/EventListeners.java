package com.calderaminecraft.tempspawn;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class EventListeners implements Listener {
    @EventHandler (priority = EventPriority.HIGHEST)
    public void PlayerRespawnEvent(PlayerRespawnEvent event) {
        if (Main.temporarySpawn == null) return;
        Player player = event.getPlayer();
        if (Main.players.contains(player.getUniqueId())) {
            if (Main.delayLoc != null) {
                event.setRespawnLocation(Main.delayLoc);
                Main.delayedPlayers.put(player.getUniqueId(), (Main.delay * 1000) + System.currentTimeMillis());
                player.sendMessage(ChatColor.RED + "Your respawn is being delayed, you will be teleported to the spawn in " + Main.delay + (Main.delay == 1 ? " second." : " seconds."));
            } else {
                event.setRespawnLocation(Main.temporarySpawn);
            }
        }
    }

    @EventHandler
    public void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();

        String command;
        if (message.contains(" ")) {
            command = message.substring(1, message.indexOf(" "));
        } else {
            command = message.substring(1);
        }

        if (Main.delayedPlayers.containsKey(player.getUniqueId()) && !Main.plugin.getConfig().getStringList("commands").contains(command)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your respawn is being delayed, you may not use this command until you respawn.");
        }
    }
}
